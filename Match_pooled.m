function [vx,vy,match_cost,time_count,match_param] = Match_pooled(patch_fea1, patch_fea2, pix_fea1 , pix_fea2)

level_num = 3;

node_deformation_coeff = 0.5*0.05;% smaller setting means more flexiable
node_truncate_const = 0.5; %0.5

patch_deform_coeff =  0.25; %0.25; %0.1
pixel_deform_coeff =  0.1; %0.1
node_search_times = 0.5;  %0.5;
pixel_search_times = 0.5; %0.25;
use_pixel = 1; %switch for pixel matching

leaf_num = 2 ^(level_num-1);
[h1, w1, d1] = size(patch_fea1);
[h2, w2, d2] = size(patch_fea2);
minsize_h = min(h1,h2);
minsize_w = min(w1,w2);

average_differ = patch_fea1(1:minsize_h,1: minsize_w, : ) - patch_fea2(1:minsize_h,1: minsize_w, : );
average_differ = average_differ.^2;
average_sum = sum(average_differ,3);
average_sum = sqrt(average_sum);
average_sum = sum(average_sum,1);
average_sum = sum(average_sum,2);
average_const = floor(average_sum/(minsize_h*minsize_w));

%%% Grid-layer
% parameters
n_init_target_feat = h1*w1; 
init_grid_size = 1; %floor(sqrt(h1*w1/n_init_target_feat) + 0.5); %1
params.n_init_target_feat = n_init_target_feat;
params.grid_size = init_grid_size;
params.n_tree_level = level_num;

sx = max(w1, w2);
sy = max(h1, h2);
params.search_radius = [ceil(sx/2), ceil(sy/2)]; 
params.truncate_const = average_const;  

tic,
[node_cost, node_trans, aux_info] = SPNodeMatchOriMex(patch_fea1, patch_fea2, params);
time_count.comp_nodecost = toc;

feature1 =pix_fea1;
feature2 =pix_fea2;
% save('dspmatch.mat','node_cost','node_trans','aux_info','params'); 
for i = 1 : params.n_tree_level
    offset = (4^(i-1)-1)/3;
    s_idx = offset + 1;
    e_idx = offset + 4^(i-1);
    x = node_cost(:, s_idx:e_idx);
    node_cost(:, s_idx:e_idx) = node_cost(:, s_idx:e_idx)./mean(x(:));
end
%%%%% BP
bp_params.deformation_coeff =  node_deformation_coeff;  
bp_params.truncate_const = node_truncate_const; 
bp_params.max_iter = 50;
bp_params.n_tree_level = params.n_tree_level;
bp_params.n_xstate = aux_info(1);
bp_params.n_ystate = aux_info(2);

tic,
opt_state = SPBPMex(single(node_cost), bp_params);
time_count.comp_gridnodematch = toc;

opt_state =  floor(opt_state);
[xi,yi] = find(opt_state <0 ); 
IND = sub2ind(size(opt_state),xi,yi);
opt_state(IND)= 0;




%%% patch-layer
% initialize from grid-layer solution
n_leaf = 4^(params.n_tree_level-1);
node_disp = node_trans(:, opt_state+1);

leaf_node_disp = node_disp(:, end-n_leaf+1:end);
n_bin = 2^(params.n_tree_level-1);
x = linspace(1, w1, n_bin+1);
y = linspace(1, h1, n_bin+1);
x = floor(x);
y = floor(y);
pool_disparity_x = zeros(h1,w1, 'int32');
pool_disparity_y = zeros(h1,w1, 'int32');
for i = 1 : n_bin
    for j = 1 : n_bin
        node_idx = j + n_bin*(i-1);
        lx = x(i);
        rx = x(i+1);
        ty = y(j);
        dy = y(j+1);
        pool_disparity_x(ty:dy,lx:rx) = leaf_node_disp(1, node_idx);
        pool_disparity_y(ty:dy,lx:rx) = leaf_node_disp(2, node_idx);
    end
end
in_disp = zeros(2, w1*h1, 'int32');
in_disp(1,:) = pool_disparity_x(:)';
in_disp(2,:) = pool_disparity_y(:)';


ndm_params.truncate_const = params.truncate_const;
ndm_params.search_grid_size = 1;  
noderx = ceil((sx/leaf_num)*node_search_times); 
nodery = ceil((sx/leaf_num)*node_search_times);
ndm_params.search_radius = floor([noderx, nodery]);
ndm_params.deform_coeff =  patch_deform_coeff;  

tic,
pool_out_disp = PixelMatchMex(patch_fea1, patch_fea2, in_disp, ndm_params);
time_count.comp_nodematch = toc;


%%%% pixel-layer
[ph1, pw1, ~] = size(pix_fea1);
[ph2, pw2, ~] = size(pix_fea2);

x = linspace(1, pw1, w1+1);
y = linspace(1, ph1, h1+1);
x = floor(x);
y = floor(y);
pixel_disparity_x = zeros(ph1,pw1, 'int32');
pixel_disparity_y = zeros(ph1,pw1, 'int32');
pixel_step_w = (pw1/w1);
pixel_step_h = (ph1/h1);
% initialize from patch-layer solution
for i = 1 : w1  %width
    for j = 1 : h1  %height
        node_idx = j + h1*(i-1);
        lx = x(i);
        rx = x(i+1);
        ty = y(j);
        dy = y(j+1);
        pixel_disparity_x(ty:dy,lx:rx) = floor(pixel_step_w *pool_out_disp(1, node_idx));
        pixel_disparity_y(ty:dy,lx:rx) = floor(pixel_step_h *pool_out_disp(2, node_idx));
    end
end
in_disp = zeros(2, pw1*ph1, 'int32');
in_disp(1,:) = pixel_disparity_x(:)';
in_disp(2,:) = pixel_disparity_y(:)';

pxm_params = ndm_params;
pxm_params.search_grid_size = 1;
pixelrx = ceil((pw1/w1)*pixel_search_times);  
pixelry = ceil((ph1/h1)*pixel_search_times);
pxm_params.search_radius = floor([pixelrx, pixelry]);
pxm_params.deform_coeff = pixel_deform_coeff; 
display('pixel  matching................');

tic, 
if use_pixel
    [out_disp, match_cost] = PixelMatchMex(feature1, feature2, in_disp, pxm_params);
    match_cost = reshape(match_cost, [ph1, pw1]);
else
    out_disp = in_disp;
    match_cost =0;
end
time_count.comp_pixelmatch = toc;


% output
time_count.comp_matchtime = time_count.comp_pixelmatch + time_count.comp_nodematch + time_count.comp_gridnodematch;

match_param.bp_params = bp_params;
match_param.ndm_params = ndm_params;
match_param.pxm_params = pxm_params;

vx = reshape(out_disp(1,:), [ph1, pw1]);
vy = reshape(out_disp(2,:), [ph1, pw1]);
vx = double(vx);
vy = double(vy);

end

