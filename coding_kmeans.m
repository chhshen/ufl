function im_codes = coding_kmeans(im, dic_first, M,P,whitening)

    pad_size = floor((dic_first.patchsize -1)/2);
    im1_padded = padarray(im, [pad_size pad_size], 'symmetric');
    im = im1_padded;

        if size(im,3) == 1
           X = im2colstep(im, [dic_first.patchsize dic_first.patchsize], [1 1] );
        else
        %   X = im2colstep(im, [dic_first.patchsize dic_first.patchsize size(im,3)], [1 1 1] );
            stepsize = dic_first.stepsize;
             stepnum = dic_first.stepnum;
             X = im2col5step(im,  [dic_first.patchsize dic_first.patchsize size(im,3)] , [1 1 1   stepsize stepsize 1   stepnum stepnum   size(im,3) ]);
        end

        % normalize for contrast
        patches = X';
        centroids = dic_first.dic';  
        patches = bsxfun(@rdivide, bsxfun(@minus, patches, mean(patches,2)), sqrt(var(patches,[],2)+10));
      
        % whiten
        if (whitening)
          patches = bsxfun(@minus, patches, M) * P;
        end
        
        % compute 'triangle' activation function
        xx = sum(patches.^2, 2);
        cc = sum(centroids.^2, 2)';
        xc = patches * centroids';
        
        zz = bsxfun(@plus, cc, bsxfun(@minus, xx, 2*xc)) ;
        [zero_x,zero_y] = find(zz<0);
        accor_zz = sub2ind(size(zz),zero_x,zero_y);  
        zz(accor_zz) = 0;
        z = sqrt( zz ); % distances
            
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
  %kmean triangular 

        [v,inds] = min(z,[],2);
        mu = mean(z, 2); % average distance to centroids for each patch
        sub_mu = bsxfun(@minus, mu, z);
        patches_kt = max( sub_mu, 0);        
        im_codes.codes = sparse( patches_kt' );
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        
        im_codes.height = size(im,1)-dic_first.patchsize+1;
        im_codes.width = size(im,2)-dic_first.patchsize+1;

end

