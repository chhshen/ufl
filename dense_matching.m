function [ vx , vy,time_count,match_param] = dense_matching(im1,im2,im_codes1 , im_pooling1 ,im_codes2 , im_pooling2)

feature = im_pooling1;
codes1f = feature.pooling; %codes codes_sc  codes_kt  codes_omp
codes1f_size.height = feature.height;
codes1f_size.width = feature.width;

codes1f = single( reshape(full(codes1f)', [ codes1f_size.height, codes1f_size.width, size(codes1f,1)]));
codes1f = single( full(codes1f));

feature = im_codes1;
codes1s = feature.codes;            

codes1s_size.height = feature.height;
codes1s_size.width = feature.width;        

codes1s = single( reshape(full(codes1s)', [ codes1s_size.height, codes1s_size.width, size(codes1s,1)]));

height1 = max(codes1f_size.height,codes1s_size.height);
width1 =  max(codes1f_size.width,codes1s_size.width);
im1 = cut_image(im1,height1, width1); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
feature = im_pooling2;
codes2f = feature.pooling;

codes2f_size.height = feature.height;
codes2f_size.width = feature.width;

codes2f = single( reshape(full(codes2f)', [ codes2f_size.height, codes2f_size.width, size(codes2f,1)]));
codes2f = single( full(codes2f));

feature = im_codes2;
codes2s = feature.codes;

codes2s_size.height = feature.height;
codes2s_size.width = feature.width;        

height2 = max(codes2f_size.height,codes2s_size.height);
width2 =  max(codes2f_size.width,codes2s_size.width);

codes2s = single( reshape(full(codes2s)', [ codes2s_size.height, codes2s_size.width, size(codes2s,1)]));
im2 = cut_image(im2,height2, width2);   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[vx,vy,match_cost,time_count,match_param] = Match_pooled(codes1f,codes2f, codes1s , codes2s); 

disp('matching data done .........');

end



function [im_f] = cut_image(im,fh,fw)

    im_h = size(im,1);
    im_w = size(im,2);
    im_d = size(im,3);

    if(fh< im_h)
        c_h = 1;
    else
        c_h = 0;    
    end

    if(fw< im_w)
        c_w = 1;    
    else
        c_w = 0;    
    end
            bbox1(3) =    (size(im,1)- fh)/2 +1;        
            bbox1(4) = fh + bbox1(3) -1;
            bbox1(1) = bbox1(3);
            bbox1(2) = fw  +  bbox1(1)-1;

    if c_h*c_w == 1
        im_f = im(int16(bbox1(3):(bbox1(4))), int16(bbox1(1):(bbox1(2))), :);
    else
        im_f =im;
    end


end
















