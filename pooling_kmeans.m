function im_pooling = pooling_kmeans(omp_codes, pooling_sz)

pooling_size = pooling_sz;

if pooling_size == 1
   im_pooling.pooling = omp_codes.codes;
   im_pooling.height = omp_codes.height;
   im_pooling.width = omp_codes.width;
else
       
   
   mod_y = mod(omp_codes.height,pooling_size);
   mod_x = mod(omp_codes.width,pooling_size);
   
   if mod_y ~= 0
     padsize_y = ceil((pooling_size - mod_y)/2);
   else
       padsize_y = 0;
   end
   
   if mod_x ~= 0
       padsize_x = ceil((pooling_size - mod_x)/2);
   else
       padsize_x =0;
   end
   
   py = floor((omp_codes.height+ 2*padsize_y)/pooling_size);
   px = floor((omp_codes.width+ 2*padsize_x)/pooling_size);
  
   codes_h = omp_codes.height + 2*padsize_y;
   codes_w = omp_codes.width + 2*padsize_x;
      
   ind_b = [];
   for i = 1:pooling_size
       ind_b = [ind_b; (1:pooling_size)'+(i-1)*codes_h];
   end
   
   % pooling
   pooling = (zeros(size(omp_codes.codes,1),px*py));
   reshape_code = reshape(full(omp_codes.codes)', [ omp_codes.height, omp_codes.width, size(omp_codes.codes,1)]);
   codes_padded = padarray( reshape_code , [padsize_y   padsize_x], 'symmetric');
   codes_padded = reshape(codes_padded , [  ( codes_h* codes_w ), size(omp_codes.codes,1)])';
   
   for i = 1:px
       for j = 1:py
           ind_s = pooling_size*(j-1)+(i-1)*pooling_size*codes_h;
           ind_p = ind_b + ind_s;
           pooling(:,(i-1)*py+j) = max(codes_padded(:,ind_p(:)),[],2);
       end
   end
   A = sparse(pooling);
   clear pooling;
   im_pooling.pooling = (A);
   im_pooling.height = py;
   im_pooling.width = px;

    
end


end

