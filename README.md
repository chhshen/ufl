Code for the following work:

#Unsupervised Feature Learning for Dense Correspondences across Scenes


paper available at: <http://arxiv.org/abs/1501.00642>
and [here](https://bytebucket.org/chhshen/ufl/raw/21dc24fdae21e1672378f1a5265c304d423d9a96/IJCV.pdf)

~~~
We propose a fast, accurate matching method for estimating dense pixel
correspondences across scenes. It is a challenging problem to estimate dense
pixel correspondences between images depicting different scenes or instances of
the same object category. While most such matching methods rely on hand-crafted
features such as SIFT, we learn features from a large amount of unlabeled image
patches using unsupervised learning. Pixel-layer features are obtained by
encoding over the dictionary, followed by spatial pooling to obtain patch-layer
features. The learned features are then seamlessly embedded into a multi-layer
match- ing framework. We experimentally demonstrate that the learned features,
together with our matching model, outperforms state-of-the-art methods such as
the SIFT flow, coherency sensitive hashing and the recent deformable spatial
pyramid matching methods both in terms of accuracy and computation efficiency.
Furthermore, we evaluate the performance of a few different dictionary learning
and feature encoding methods in the proposed pixel correspondences estimation
framework, and analyse the impact of dictionary learning and feature encoding
with respect to the final matching performance.
~~~

If this code is useful for your research, please consider to cite our work:

~~~
@article{Zhang2015IJCV,
    author = {Chao Zhang  and Chunhua Shen  and  Tingzhi Shen},
    title  = {Unsupervised Feature Learning for Dense Correspondences across Scenes},
    journal= {Int. J. Computer Vision},
    volume = {},
    number = {},
    year   = {2015},
    url    = {http://arxiv.org/abs/1501.00642},
    month  = {},
    pages  = {},
}
~~~






1) `demo_dic_kmeans_11x11_sn11_dic100_rgb.mat` is the dictionary for encoding the pixel feature, which is learned by kmeans from background_google images of the Caltech101 dataset. Test images can be encoded by using this learnt dictionary.

2) Folder `cpp` contains the files for the loopy belief propagation algorithm, which are taken from http://vision.cs.utexas.edu/projects/dsp. Credit goes to the original authors.

3) Run `demo_one.m` to try our dense maching algorithm.

`demo_one(im1_name,im2_name);`

4) This code have been tested under Windows8 64bit.














#Contact
authors: Chao Zhang, Chunhua Shen

email: chunhua.shen@adelaide.edu.au

#Copyright

Copyright (c) The authors, 2015.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.

For commercial applications, please contact Chunhua Shen <http://www.cs.adelaide.edu.au/~chhshen/>.


05/2015



